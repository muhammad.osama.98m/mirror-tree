        typedef int ElementType;

/* START: fig4_16.txt */
        #ifndef _Tree_H
        #define _Tree_H

        struct TreeNode;
        typedef struct TreeNode *Position;
        typedef struct TreeNode *SearchTree;

        SearchTree MakeEmpty( SearchTree T );
        Position Find( ElementType X, SearchTree T );      //cannot find sub-branches
        Position findRight( ElementType X, SearchTree T ); //can find sub-branches
        Position findLeft( ElementType X, SearchTree T );  //can find sub-branches
        void printInorder (SearchTree T);				   //tree traversal function header
        void printPreorder (SearchTree T);
        void printPostorder(SearchTree T);
        void MirrorTree(Position T);
        Position FindMin( SearchTree T );
        Position FindMax( SearchTree T );
        SearchTree Insert( ElementType X, SearchTree T );
		SearchTree autoInsert(ElementType X[], SearchTree T, ElementType index, ElementType size); //generic auto inserting function header
        SearchTree Delete( ElementType X, SearchTree T );
        ElementType Retrieve( Position P );
        ElementType getLevel(SearchTree T, ElementType value, ElementType level);
        ElementType isolatorFunc(SearchTree T, ElementType value); //isolate local variables in recursive function runtime
        ElementType isLeaf(Position T, ElementType X); //leaf node checker

        #endif  /* _Tree_H */

/* END */
