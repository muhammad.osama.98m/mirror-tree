        #include "../include/tree.h"
        #include "../include/fatal.h"
        #include <stdlib.h>

        struct TreeNode
        {
            ElementType Element;
            SearchTree  Left;
            SearchTree  Right;
        };

/* START: fig4_17.txt */
        SearchTree
        MakeEmpty( SearchTree T )
        {
            if( T != NULL )
            {
                MakeEmpty( T->Left );
                MakeEmpty( T->Right );
                free( T );
            }
            return NULL;
        }
/* END */

/* START: fig4_18.txt */
        Position
        Find( ElementType X, SearchTree T )
        {
            if( T == NULL )
                return NULL;
            if( X < T->Element )
            {
                return Find( X, T->Left );
            }
            else
            if( X > T->Element )
            {
                return Find( X, T->Right );
            }
            else
            {
                return T;
            }
        }
/* END */

/* START: fig4_18.txt */
        Position
        findRight( ElementType X, SearchTree T )
        {
            if( T == NULL )
            {
                return NULL; //if BST is empty, return nothing
            }
            else if(T->Element == X)
            {
                return T; //if value found return its position
            }

            return findRight(X, T->Right); //iterate recursively
        }

        Position
        findLeft( ElementType X, SearchTree T )
        {
            if( T == NULL )
            {
                return NULL;
            }
            else if(T->Element == X)
            {
                return T;
            }

            return findLeft(X, T->Left);
        }
/* END */

/* START: fig4_19.txt */
        Position
        FindMin( SearchTree T )
        {
            if( T == NULL )
                return NULL;
            else
            if( T->Left == NULL )
                return T;
            else
            {
                //printf("%d\n", Retrieve(T->Left));
                return FindMin( T->Left );
            }
        }
/* END */

/* START: fig4_20.txt */
        Position
        FindMax( SearchTree T )
        {
            if( T != NULL )
                while( T->Right != NULL)
                {
                    //printf("%d\n", Retrieve(T->Right));
                    T = T->Right;
                }


            return T;
        }
/* END */

/* START: fig4_22.txt */
        SearchTree
        Insert( ElementType X, SearchTree T )
        {
/* 1*/      if( T == NULL )
            {
                /* Create and return a one-node tree */
/* 2*/          T = malloc( sizeof( struct TreeNode ) );
/* 3*/          if( T == NULL )
/* 4*/              FatalError( "Out of space!!!" );
                else
                {
/* 5*/              T->Element = X;
/* 6*/              T->Left = T->Right = NULL;
                }
            }
            else
/* 7*/      if( X < T->Element )
/* 8*/          T->Left = Insert( X, T->Left );
            else
/* 9*/      if( X > T->Element )
/*10*/          T->Right = Insert( X, T->Right );
            /* Else X is in the tree already; we'll do nothing */

/*11*/      return T;  /* Do not forget this line!! */
        }
/* END */

		SearchTree autoInsert(ElementType X[], SearchTree T, ElementType index, ElementType size)
		{
			if(T != NULL)
			{
				printf("Make BST empty first!\n"); //base case prompt
				return 0;
			}
			else if(index < size)
				{
					T = malloc( sizeof( struct TreeNode ) );
					if( T == NULL )
						FatalError( "Out of space!!!" );
					else
						{
							T->Element = X[index]; //first entry is made the root node
							T->Left = T->Right = NULL;
						}

					T->Left = autoInsert(X, T->Left, 2 * index + 1, size); //place left child nodes

					T->Right = autoInsert(X, T->Right, 2 * index + 2, size); //place right child nodes
				}
			return T; //return the BST, VERY IMPORTANT!
		}

/* START: fig4_25.txt */
        SearchTree
        Delete( ElementType X, SearchTree T )
        {
            Position TmpCell;

            if( T == NULL )
                Error( "Element not found" );
            else
            if( X < T->Element )  /* Go left */
                T->Left = Delete( X, T->Left );
            else
            if( X > T->Element )  /* Go right */
                T->Right = Delete( X, T->Right );
            else  /* Found element to be deleted */
            if( T->Left && T->Right )  /* Two children */
            {
                /* Replace with smallest in right subtree */
                TmpCell = FindMin( T->Right );
                T->Element = TmpCell->Element;
                T->Right = Delete( T->Element, T->Right );
            }
            else  /* One or zero children */
            {
                TmpCell = T;
                if( T->Left == NULL ) /* Also handles 0 children */
                    T = T->Right;
                else if( T->Right == NULL )
                    T = T->Left;
                free( TmpCell );
            }

            return T;
        }
/* END */

        ElementType
        Retrieve( Position P )
        {
            return P->Element;
        }

        void printInorder (SearchTree T) //basic recursive function
        {
            if(T == NULL)
                return;
            printInorder(T->Left); //reccur left
            printf("%d, ", T->Element); //print node data
            printInorder(T->Right); //reccur right
        }

        void printPreorder (SearchTree T)
        {
            if(T == NULL)
                return;
            printf("%d, ", T->Element);
            printPreorder( T->Left );
            printPreorder( T->Right );
        }

        void printPostorder (SearchTree T)
        {
            if(T == NULL)
                return;
            printPostorder( T->Left );
            printPostorder( T->Right );
            printf("%d, ", T->Element);
        }

        ElementType getLevel(SearchTree T, ElementType value, ElementType level)
        {
            if (T == NULL) //base case
                return 0;

            if (T->Element == value)
            {
                return level; //return level if value matches
            }

            int levelDepth = getLevel(T->Left, value, level + 1); //reccur left
            if (levelDepth != 0)
                return levelDepth;

            levelDepth = getLevel(T->Right, value, level + 1); //reccur right
            return levelDepth;
        }

        ElementType isLeaf(SearchTree T, ElementType X)
        {

            if(T->Element == X)
            {
                if((T->Left == NULL) && (T->Right == NULL)) //leaf node check, leaf nodes have no child nodes.
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
            }
            
            return 0;
        }

        ElementType isolatorFunc(Position T, ElementType value) //isolate local variable to prevent overwrite
        {
            if(findLeft(value, T) != NULL) //reccur left
            {
                if(isLeaf(findLeft(value, T), value) == 1)
                {
                    return getLevel(T, value, 1);
                }
                else
                    return 0; //base case
            }
            if(findRight(value, T) != NULL) //reccur right
            {
                if(isLeaf(findRight(value, T), value) == 1)
                {
                    return getLevel(T, value, 1);
                }
                else
                    return 0; //base case
            }

            return getLevel(T, value, 1); //reccur branches

        }
        
        void MirrorTree(Position T) {
			
			if(T == NULL) {
				
				return;
			}
			
			Position temp;
			
			MirrorTree(T->Left);
			MirrorTree(T->Right);
			
			temp = T->Left;
			T->Left = T->Right;
			T->Right = temp;
		}
