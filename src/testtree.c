#include "../src/tree.c"
#include <stdio.h>
#include <string.h>

#define		SIZE	10
#define		MIN 	0
#define		MAX 	100

int main( )
{
    SearchTree T;
    //Position P;

    T = MakeEmpty( NULL );

    /*T = Insert( 1, T );
    T = Insert( 2, T );
    T = Insert( 3, T );
    T = Insert( 4, T );
    T = Insert( 5, T );*/

    int seed = 64;
	int X[SIZE],k;
	srand(seed);

	for(k = 0; k < SIZE; k++) {
		X[k] = rand() % (MAX - MIN + 1) + MIN;
		T = Insert(X[k], T);  //uncomment for removing duplicate entries. Only one input method can be active at a time.
	}

	//int X[] = {1, 2, 3, 4, 5};
	//int size = sizeof(X)/sizeof(X[0]);

    //T = autoInsert(X, T, 0, SIZE); //allow duplicate entries and generic inputs. Only one input method can be active at a time.

    puts("BST inorder traversal...\n");
    printInorder(T);
    puts("\n\nBST preorder traversal...\n");
    printPreorder(T);
    puts("\n\nBST postorder traversal...\n");
    printPostorder(T);
    puts("\n\n");
    
    MirrorTree(T);	//invert or mirror BST
    printf("Mirrored/Inverted Tree in Inorder...\n\n");
    printInorder(T);
    puts("\n\n");

    return 0;
}

